<?php

namespace App\Http\Controllers;

use App\Post;
use App\Video;
use App\Package;
use App\Service;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $main_banner_class = '';
        $packages = Package::all()->take(4);
        $videos = Video::all()->take(4);
        $posts = Post::all()->take(3);
        $services = Service::all()->take(3);
        return view('welcome', compact(['main_banner_class', 'videos', 'packages', 'services', 'posts']));
    }
}
