<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/pakety', 'PackageController@index')->name('packages');
Route::get('/video', 'VideoController@index')->name('videos');
Route::get('/uslugi', 'ServiceController@index')->name('services');
Route::get('/publikatsii', 'PostController@index')->name('posts');
