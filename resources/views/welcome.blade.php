@extends('layouts.app')

@section('content')
    <div class="container">
        <section id="quote">
            <div class="container">
                <p>«Слабые желания приносят слабые результаты. Равно как небольшое количество огня дает небольшое количество тепла».</p>
                <div>— Наполеон Хилл</div>
            </div>
        </section>
    </div>
    <section class="link-row mb-5">
        <a href="">Видео-курс «Как зарабатывать большие деньги на любимом деле» <i class="fas fa-chevron-circle-right"></i></a>
    </section>
    <div class="container">
        <section id="packages" class="section">
            <h2 class="font-weight-bold mb-4">Пакеты</h2>
            @if($packages->count() > 0)
                <div class="row">
                    @foreach($packages as $package)
                        <div class="col-md-4">
                            <div class="package shadow-sm">
                                <a href="{{ route('packages') }}/{{ $package->url }}"><div class="package-img" style="background-image:url({{ $package->img }});"></div></a>
                                <div class="package-text">
                                    <a href="{{ route('packages') }}/{{ $package->url }}"><div class="h5 font-weight-bold">{{ $package->title }}</div></a>
                                    <p>{{ $package->description }}</p>
                                    <div class="old-price">{{ $package->old_price }} тг.</div>
                                    <a href="{{ route('packages') }}/{{ $package->url }}" class="btn btn-warning my-btn">{{ $package->price }} тг.</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="h3 font-weight-light mb-5" style="min-height:200px;">Нет пакетов.</div>
            @endif
            <a href="{{ route('packages') }}"><i class="fas fa-gift"></i> <div class="h4 main-h4 mt-4">Все пакеты</div></a>
        </section>
        <hr>
        <section id="webinars" class="section">
            <h2 class="font-weight-bold mb-4">Видео и вебинары</h2>
            @if($videos->count() > 0)
                <div class="row">
                    @foreach($videos as $video)
                        <div class="col-md-3 mb-3">
                            <div class="webinar">
                                <div class="webinar-img-holder">
                                    <a href="{{ route('videos') }}/{{ $video->url }}"><div class="webinar-img" style="background-image: url({{ asset('img/' . $video->img) }})">
                                        <div class="webinar-play"></div>
                                    </div></a>
                                </div>
                                <div class="webinar-title mb-3">{{ $video->title }}</div>
                                <a href="{{ route('videos') }}/{{ $video->url }}" class="btn btn-warning my-btn">{{ $video->price }} тг.</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="h3 font-weight-light mb-5" style="min-height:200px;">Нет видео.</div>
            @endif

            <a href="{{ route('videos') }}"><i class="fas fa-play-circle"></i> <div class="h4 main-h4 mt-4">Все видео</div></a>
        </section>
        <hr>
        <section id="psycho" class="section">
            <h2 class="font-weight-bold mb-4">Услуги психолога</h2>
            @if($services->count() > 0)
                <div class="row">
                    @foreach($services as $service)
                        <div class="col-md-4">
                            <a href="{{ route('services') }}/{{ $service->url }}"><div class="psycho-service">
                                <div class="h4">{{ $service->title }}</div>
                                <div class="subtext">({{ $service->sub_title }})</div>
                                <div class="psycho-service-img" style="background-image: url({{ asset('img/' . $service->img) }})"></div>
                                <p>{{ $service->description }}</p>
                            </div></a>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="h3 font-weight-light mb-5" style="min-height:200px;">Нет услуг.</div>
            @endif
            <a href="{{ route('services') }}"><i class="fas fa-box-open"></i> <div class="h4 main-h4 mt-4">Все услуги</div></a>
        </section>
    </div>
    <section id="book" class="section bg-gray">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <img src="{{ asset('img/book.png') }}" width="100%">
                </div>
                <div class="col-md-6">
                    <div class="book-text">
                        Книга<br> «Тонкое искусство пофигизма»
                    </div>
                    <a href="" class="btn btn-warning my-btn">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="link-row mb-5">
        <a href="">Индивидуальная программа <i class="fas fa-chevron-circle-right"></i></a>
    </section>
    <div class="container">
        <section id="posts" class="section">
            @if($posts->count() > 0)
            <div class="row">
                @foreach($posts as $post)
                <div class="col-md-6">
                    <a href="{{ route('posts') }}/{{ $post->url }}"><div class="post-item">
                        <div class="h4">{{ $post->title }}</div>
                        <p>{{ mb_strcut($post->description, 0, 255) }}... <i class="fas fa-chevron-circle-right text-warning"></i></p>
                        <div class="post-img" style="background-image: url({{ asset('img/' . $post->img) }})"></div>
                    </div></a>
                </div>
                @endforeach
            </div>
            @else
                <div class="h3 font-weight-light mb-5" style="min-height:200px;">Нет публикаций.</div>
            @endif
            <a href="{{ route('posts') }}"><i class="fas fa-clone"></i> <div class="h4 main-h4 mt-4">Все публикации</div></a>
        </section>
        <hr>
        <section class="section text-center">
            <div class="global-wrapper">
                <a href="" class="tag-cloud-link" style="font-size: 1.0881679389313em;">Деньги</a>
                <a href="" class="tag-cloud-link" style="font-size: 1.0881679389313em;">Дети</a>
                <a href="" class="tag-cloud-link" style="font-size: 1.0484732824427em;">Зависимости</a>
                <a href="" class="tag-cloud-link" style="font-size: 0.93931297709924em;">Законы Вселенной</a>
                <a href="" class="tag-cloud-link" style="font-size: 1.0881679389313em;">Здоровье</a>
                <a href="" class="tag-cloud-link" style="font-size: 0.93931297709924em;">Конфликты</a>
                <a href="" class="tag-cloud-link" style="font-size: 0.85em;">Либидо</a>
                <a href="" class="tag-cloud-link" style="font-size: 0.85em;">Манипуляция</a>
                <a href="" class="tag-cloud-link" style="font-size: 1.0484732824427em;">Мужчины</a>
                <a href="" class="tag-cloud-link" style="font-size: 0.99885496183206em;">Нарциссизм</a>
                <a href="" class="tag-cloud-link" style="font-size: 0.85em;">Одиночество</a>
                <a href="" class="tag-cloud-link" style="font-size: 1.4206106870229em;">Отношения</a>
                <a href="" class="tag-cloud-link" style="font-size: 1.5em;">Психология</a>
                <a href="" class="tag-cloud-link" style="font-size: 1.2171755725191em;">Психосоматика</a>
                <a href="" class="tag-cloud-link" style="font-size: 1.3114503816794em;">Родительство</a>
                <a href="" class="tag-cloud-link" style="font-size: 0.93931297709924em;">Свобода</a>
                <a href="" class="tag-cloud-link" style="font-size: 0.93931297709924em;">Секс</a>
                <a href="" class="tag-cloud-link" style="font-size: 0.85em;">Смысл жизни</a>
                <a href="" class="tag-cloud-link" style="font-size: 0.93931297709924em;">Страхи</a>
                <a href="" class="tag-cloud-link" style="font-size: 1.0484732824427em;">Счастье</a>
                <a href="" class="tag-cloud-link" style="font-size: 0.85em;">Утрата</a>
                <a href="" class="tag-cloud-link" style="font-size: 0.85em;">Энергия</a>
            </div>
        </section>
    </div>
@endsection