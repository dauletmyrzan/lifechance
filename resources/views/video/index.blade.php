@extends('layouts.app')

@section('content')
    <div class="container">
        <section id="videos" class="section">
            <h1 class="mb-4">Видео и вебинары</h1>
            @if($videos->count() > 0)
                <div class="row">
                    @foreach($videos as $video)
                        <div class="col-md-3 mb-3">
                            <div class="webinar">
                                <div class="webinar-img-holder">
                                    <a href="{{ route('videos') }}/{{ $video->url }}"><div class="webinar-img" style="background-image: url({{ asset('img/' . $video->img) }})">
                                        <div class="webinar-play"></div>
                                    </div></a>
                                </div>
                                <div class="webinar-title mb-3">{{ $video->title }}</div>
                                <a href="{{ route('videos') }}/{{ $video->url }}" class="btn btn-warning my-btn">{{ $video->price }} тг.</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="h3 font-weight-light mb-5" style="min-height:200px;">Нет видео.</div>
            @endif
        </section>
    </div>
@endsection