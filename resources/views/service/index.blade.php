@extends('layouts.app')

@section('content')
    <div class="container">
        <section id="psycho" class="section">
            <h1 class="mb-5">Услуги психолога</h1>
            @if($services->count() > 0)
                <div class="row">
                    @foreach($services as $service)
                        <div class="col-md-4">
                            <a href="{{ route('services') }}/{{ $service->url }}"><div class="psycho-service">
                                    <div class="h4 text-left">{{ $service->title }}</div>
                                    <div class="subtext text-left">({{ $service->sub_title }})</div>
                                    <div class="psycho-service-img" style="background-image: url({{ asset('img/' . $service->img) }})"></div>
                                    <p>{{ $service->description }}</p>
                                </div></a>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="h3 font-weight-light mb-5" style="min-height:200px;">Нет услуг.</div>
            @endif
        </section>
    </div>
@endsection