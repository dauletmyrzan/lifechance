@extends('layouts.app')

@section('content')
<div class="container">
	<section id="packages" class="section">
        <h1 class="mb-4">Пакеты</h1>
        <div class="row mb-4">
            <div class="col-md-7">
                <p>После оплаты вам на почту придет письмо со ссылками на видео для просмотра онлайн. Ссылки будут действительны на указанное количество дней с момента оплаты.</p>
            </div>
        </div>
        @if($packages->count() > 0)
        <div class="row">
        	@foreach($packages as $package)
            <div class="col-md-4">
                <div class="package shadow-sm">
                    <a href="{{ route('packages') }}/{{ $package->url }}"><div class="package-img" style="background-image:url('{{ $package->img }}');"></div></a>
                    <div class="package-text">
                        <a href="{{ route('packages') }}/{{ $package->url }}"><div class="h5 font-weight-bold">{{ $package->title }}</div></a>
                        <p>{{ $package->description }}</p>
                        <div class="old-price">{{ $package->old_price }} тг.</div>
                        <a href="{{ route('packages') }}/{{ $package->url }}" class="btn btn-warning my-btn">{{ $package->price }} тг.</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @else
        <div class="h3 font-weight-light mb-5" style="min-height:200px;">Нет пакетов.</div>
        @endif
    </section>
</div>
@endsection