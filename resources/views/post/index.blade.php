@extends('layouts.app')

@section('content')
    <div class="container">
        <section id="posts" class="section">
            <h1 class="mb-5">Публикации</h1>
            @if($posts->count() > 0)
                <div class="row">
                    @foreach($posts as $post)
                        <div class="col-md-6">
                            <a href="{{ route('posts') }}/{{ $post->url }}"><div class="post-item">
                                    <div class="h4">{{ $post->title }}</div>
                                    <p>{{ mb_strcut($post->description, 0, 255) }}... <i class="fas fa-chevron-circle-right text-warning"></i></p>
                                    <div class="post-img" style="background-image: url({{ asset('img/' . $post->img) }})"></div>
                                </div></a>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="h3 font-weight-light mb-5" style="min-height:200px;">Нет публикаций.</div>
            @endif
        </section>
    </div>
@endsection