<!DOCTYPE html>
<html lang="ru">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="author" content="Daulet Myrzan">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta charset="utf-8">

    <title>Lifechance - Трансформация. Психология. Коучинг.</title>

    <link rel="shortcut icon" href="img/icons/">

    <link href='https://fonts.googleapis.com/css?family=Raleway:200,300,400,700,900,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:200,300,400,900,700,500,300' rel='stylesheet' type='text/css'>

    <!-- ============================================= -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <script src="https://kit.fontawesome.com/2b82ad5851.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="menu-icon">
        <span class="menu-icon__line menu-icon__line-left"></span>
        <span class="menu-icon__line"></span>
        <span class="menu-icon__line menu-icon__line-right"></span>
    </div>
    <div class="nav">
        <div class="nav__content">
            <ul class="nav__list">
                <li class="nav__list-item">Главное</li>
                <li class="nav__list-item">Мероприятия</li>
                <li class="nav__list-item">Видео</li>
                <li class="nav__list-item">Пакеты</li>
                <li class="nav__list-item">Услуги психолога</li>
                <li class="nav__list-item">Консультации</li>
                <li class="nav__list-item">Контакты</li>
            </ul>
        </div>
    </div>
    <section id="main" class="banner_part {{ isset($main_banner_class) ? $main_banner_class : 'banner_part-short' }}">
        <div class="gradient"></div>
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="banner_text">
                        <div class="banner_text_iner">
                            <h5>Проект и психологический центр в Алматы</h5>
                            <h1>Трансформация</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @yield('content')
    <hr class="mb-0">
    <section class="section" id="newsletter">
        <div class="container">
            <div class="h3">Подпишись на наши новости!</div>
            <form action="" class="my-5">
                <div class="row justify-content-center">
                    <div class="col-md-4 mb-4">
                        <input type="text" class="form-control" placeholder="Имя">
                    </div>
                    <div class="col-md-4 mb-4">
                        <input type="email" class="form-control" placeholder="Email">
                    </div>
                </div>
                <button class="mt-4 btn btn-warning my-btn">Подписаться</button>
            </form>
        </div>
    </section>
    <hr class="my-0">
    <section class="section" id="contacts">
        <div class="container text-center py-5">
            <div class="h4 font-weight-light">Служба поддержки работает с 10:00 до 19:00 (по времени Астаны) в рабочие дни</div>
            <div class="h4 font-weight-normal mb-5">
                <a href="mailto:support@lifechance.ru" class="d-inline-block"><i class="far fa-envelope"></i> support@lifechance.kz</a>
            </div>
            <div class="h4 font-weight-light">По вопросам индивидуальных консультаций, а также сотрудничества<br> любого плана пишите менеджеру</div>
            <div class="h4 font-weight-normal">
                <a href="mailto:support@lifechance.ru" class="d-inline-block"><i class="far fa-envelope"></i> manager@lifechance.kz</a>
            </div>
        </div>
    </section>
    <footer class="py-5">
        <div class="container text-muted">
            <div class="row align-items-center">
                <div class="col-md-4">
                    <div>&copy; 2019 Lifechance</div>
                    <div style="font-size:10pt;">
                        Сертификат качества ГОСТ<br>
                        По вопросам оплаты:<br>
                        ИП Фомичева Марина Юрьевна. ИНН: 128938123138.<br>
                        050000, Казахстан, Алматы, ул. Абая, д. 29
                    </div>
                </div>
                <div class="col-md-4 text-center social">
                    <a href="" class="mr-3"><i class="fab fa-instagram fa-3x"></i></a>
                    <a href=""><i class="far fa-envelope fa-3x"></i></a>
                </div>
                <div class="col-md-4 text-center text-md-right">
                    <img src="https://cdn.pixabay.com/photo/2018/03/27/15/05/logo-3266214_960_720.png" width="250">
                </div>
            </div>
        </div>
    </footer>
    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/nav.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>
</body>
</html>
